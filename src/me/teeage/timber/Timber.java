package me.teeage.timber;

import java.util.LinkedList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class Timber extends JavaPlugin implements Listener {

	@Override
	public void onEnable() {
		Bukkit.getPluginManager().registerEvents(this, this);
	}

	@EventHandler
	private void onBlockBreak(BlockBreakEvent e) {
		if (!e.getPlayer().isSneaking())
			if(e.getPlayer().hasPermission("timber.use"))
				if (isAxe(e.getPlayer().getItemInHand().getType()))
					if (e.getBlock().getType() == Material.LOG || e.getBlock().getType() == Material.LOG_2)
						dropTree(e.getBlock().getLocation());

	}

	private void dropTree(final Location location) {
		List<Block> blocks = new LinkedList<>();

		for (int i = location.getBlockY(); i < location.getWorld().getHighestBlockYAt(location.getBlockX(), location.getBlockZ()); i++) {
			Location l = location.add(0, 1, 0);
			if (l.getBlock().getType() == Material.LOG || l.getBlock().getType() == Material.LOG_2)
				blocks.add(l.getBlock());
			else
				break;

			l = null;
		}
		for (Block block : blocks) {
			block.breakNaturally(new ItemStack(Material.DIAMOND_AXE));
		}
		blocks = null;
	}

	private boolean isAxe(Material material) {
		boolean isAxe;
		switch (material) {
		case WOOD_AXE:
		case STONE_AXE:
		case IRON_AXE:
		case GOLD_AXE:
		case DIAMOND_AXE:
			isAxe = true;
			break;
		default:
			isAxe = false;
			break;
		}
		return isAxe;
	}

}
